HOWTO
-----

### Build Container Images
~~~
# App1
cd app1
docker build -t app1 . -f Dockerfile.dev

# App2
cd app2
docker build -t app2 . -f Dockerfile.dev

# Nginx
cd nginx
docker build -t nginx . -f Dockerfile.dev
~~~

### Start All Containers
~~~
docker-compose up
~~~

Fire your browser and go to http://localhost. Refresh several times and check if you get response from app1, app2.

### Docker Hosting: sloppy.io
Hostnames in sloppy.io are assigned automaticaly. Each container will get unique hostname. You can find it in app settings - Advanced Tab.
